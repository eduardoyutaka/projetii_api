Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post 'user', to:              'public#signup'
  post 'login', to:             'public#login'

  constraints subdomain: 'api' do
    scope module: 'api' do
      namespace :v1 do
        resources :users
      end
    end
  end

  @version = 'api/v1/'

  scope 'api/v1' do
    get 'project', to:          @version+'users#index_project'
    get 'task/project/:id', to: @version+'users#index_task'
    get 'project/:id', to:      @version+'users#show_project'
    get 'task/:id', to:         @version+'users#show_task'
    get 'quiz/task/:id', to:    @version+'users#show_quiz'
    get 'project/:id/buy', to:  @version+'users#buy_project'
  end
end
