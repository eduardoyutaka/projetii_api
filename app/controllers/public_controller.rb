class PublicController < ApplicationController
  before_action :get_json, only:[:singup, :login]

  def signup
    @user = User.find_by(email: @json['email'])
    if @user != nil
      render status: :bad_request, json: {msg: 'Email already exists.'}
    else
      @user = User.create(email: @json['email'], password: @json['password'])
      if @user.persisted?
        render status: :ok, json: @user
      else
        render status: :unprocessable_entity, json: {msg: 'Invalid email or password.'}
      end
    end
  end

  def login
    @user = User.find_by(email: @json['email'])
    if @user != nil
      @user = User.find_by(email: @json['email'], password: @json['password'])
      if @user != nil
        render status: :ok, json: @user
      else
        render status: :unprocessable_entity, json: {msg: 'Invalid email or password.'}
      end
    else
      render status: :not_found, json: {msg: 'User not found.'}
    end
  end

  private
  def get_json
    @json = JSON.parse(request.body.read)
  end
end
