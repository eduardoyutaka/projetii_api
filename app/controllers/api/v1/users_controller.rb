module Api::V1
  class UsersController < ApiController
    def index_project
      @projects = Project.all
      if @projects != nil
        render status: :ok, json: @projects
      else
        render status: :not_found, json: {msg: 'Projects not found.'}
      end
    end

    def index_task
      @tasks = []
      @task_ids = TasksUsersJoinTable.where(project_id: params[:id], user_id: @current_user)
      @task_ids.each do |id|
        @task = Task.find(id)
        if @task != nil
          @tasks.push(@task)
        else
          render status: :not_found, json: {msg: 'Task not found.'} and return
        end
      end
      render status: :ok, json: @tasks
    end

    def show_project
      @project = Project.find_by(params[:id])
      if @project != nil
        render status: :ok, json: @project
      else
        render status: :not_found, json: {msg: 'Project not found.'}
      end
    end

    def show_task
      @task = Task.find(params[:id])
      if @task != nil
        render status: :ok, json: @task
      else
        render status: :not_found, json: {msg: 'Task not found.'}
      end
    end

    def show_quiz
      @quiz = Quiz.find_by(task_id: params[:id])
      if @quiz != nil
        render status: :ok, json: @quiz
      else
        render status: :not_found, json: {msg: 'Quiz not found.'}
      end
    end

    def buy_project
      @project = Project.find(params[:id])
      @tasks = Task.where(project_id: @project.id)
      if @tasks != nil
        @tasks.each do |task|
          TasksUsersJoinTable.create(
            project_id: params[:id],
            task_id: task.id,
            user_id: @current_user.id
          )
        end
        render status: :ok, json: {msg: 'Success.'}
      else
        render status: :not_found, json: {msg: 'Tasks not found.'} and return 
      end
    end

    private
    def get_json
      @json = JSON.parse(request.body.read)
    end
  end
end
