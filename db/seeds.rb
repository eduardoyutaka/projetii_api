# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(
  email: 'calgaro.enzo@gmail.com',
  password: 'projetii'
)

Project.create(
  detail_long: 'Com um gasto de aproximado de R$10,00 em materias, vocês podem construir algo simples e divertido.',
  detail_short: 'Aprenda a fazer seu próprio campinho de futebol para não deixar de jogar nem nos dias de chuva.',
  estimated_time: '2',
  image: 'http://www.tocadacotia.com/wp-content/uploads/2014/03/10-coisas-bem-legais-para-fazer-com-uma-caixa-de-pizza-8.jpg',
  grade: '5º ano',
  price: '6',
  task_number: '10',
  title: 'Campinho de Futebol',
  thumbnail: 'http://www.tocadacotia.com/wp-content/uploads/2014/03/10-coisas-bem-legais-para-fazer-com-uma-caixa-de-pizza-8.jpg'
)
Project.create(
  detail_long: '',
  detail_short: 'Cultivar seus próprios alimentos nunca foi tão divertido. Aprenda a plantar e colher sua próxima refeição.',
  estimated_time: '8',
  image: '',
  grade: '',
  price: '',
  task_number: '',
  title: 'Mini Horta',
  thumbnail: 'http://blog.mundohorta.com.br/wp-content/uploads/2013/10/horta-org%C3%A2nica-em-jardim-org%C3%A2nico.jpg'
)

Task.create(
  project_id: 1,
  detail: 'O futebol se joga num campo de grama natural ou sintética de forma retangular. As medidas permitidas do terreno são de 90 a 120 metros de comprimento e de 45 a 90 metros de largura, mas para partidas internacionais se recomenda as seguintes medidas: entre 100 e 110 metros de comprimento e entre 64 e 75 metros de largura.',
  estimated_time: '2',
  image: 'http://www.ebc.com.br/sites/_portalebc2014/files/atoms_image/campo_de_futebol_medidas.jpg',
  subject: 'ios-calculator',
  title: '1. O tamanho do campo'
)
Task.create(
  project_id: 1,
  detail: '',
  estimated_time: '2',
  image: '',
  subject: 'ios-calculator',
  title: '2. Aprendendo escalas'
)
Task.create(
  project_id: 1,
  detail: '',
  estimated_time: '2',
  image: '',
  subject: 'ios-color-wand',
  title: '3. Selecionando os materiais'
)
Task.create(
  project_id: 1,
  detail: '',
  estimated_time: '2',
  image: '',
  subject: 'ios-options',
  title: '4. Utilizando medidas'
)
Task.create(
  project_id: 1,
  detail: '',
  estimated_time: '2',
  image: '',
  subject: 'ios-cut',
  title: '5. Trabalhando com corte e cola'
)

TasksUsersJoinTable.create(
  project_id: 1,
  task_id: 1,
  user_id: 1
)
TasksUsersJoinTable.create(
  project_id: 1,
  task_id: 2,
  user_id: 1
)
TasksUsersJoinTable.create(
  project_id: 1,
  task_id: 3,
  user_id: 1
)
TasksUsersJoinTable.create(
  project_id: 1,
  task_id: 4,
  user_id: 1
)

Quiz.create(
  task_id: 1,
  answer: 'A',
  option_a: 'de 100 a 110 metros.',
  option_b: '54 a 60 metros.',
  option_c: '90 a 100 metros.',
  option_d: '52 a 58 metros.',
  question: 'Qual o comprimento do campo de futebol?'
)
Quiz.create(
  task_id: 2,
  answer: 'B',
  option_a: 'Opção A.',
  option_b: 'Opção B.',
  option_c: 'Opção C.',
  option_d: 'Opção D.',
  question: 'Pergunta da tarefa 2:'
)
Quiz.create(
  task_id: 3,
  answer: 'C',
  option_a: 'Opção A.',
  option_b: 'Opção B.',
  option_c: 'Opção C.',
  option_d: 'Opção D.',
  question: 'Pergunta da tarefa 3:'
)
Quiz.create(
  task_id: 4,
  answer: 'D',
  option_a: 'Opção A.',
  option_b: 'Opção B.',
  option_c: 'Opção C.',
  option_d: 'Opção D.',
  question: 'Pergunta da tarefa 4:'
)
