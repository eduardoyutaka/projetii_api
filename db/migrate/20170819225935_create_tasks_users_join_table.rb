class CreateTasksUsersJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks_users_join_tables do |t|
      t.integer :project_id
      t.integer :task_id
      t.integer :user_id
      t.boolean :done

      t.timestamps
    end
  end
end
