class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.integer :project_id
      t.text :detail
      t.float :estimated_time
      t.string :image
      t.string :subject
      t.string :title

      t.timestamps
    end
  end
end
