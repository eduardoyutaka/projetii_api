class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :detail_long
      t.string :detail_short
      t.float :estimated_time
      t.string :image
      t.string :grade
      t.float :price
      t.float :task_number
      t.string :title
      t.string :thumbnail

      t.timestamps
    end
  end
end
