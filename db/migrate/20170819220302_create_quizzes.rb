class CreateQuizzes < ActiveRecord::Migration[5.0]
  def change
    create_table :quizzes do |t|
      t.integer :task_id
      t.string :answer
      t.string :option_a
      t.string :option_b
      t.string :option_c
      t.string :option_d
      t.string :question

      t.timestamps
    end
  end
end
